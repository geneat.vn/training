RESTFUL API CONVENTION DÙNG TRONG CÁC DỰ ÁN GENEAT
===

Mục tiêu: tài liệu mô tả các convention khi thiết kế API dùng trong các dự án của GENEAT.

Mục lục

1. Mục tiêu thiết kế API
2. Cách đặt tên URL
3. Sử dụng HTTP Method
4. Response Code
5. Request/Response model
6. Một số trường hợp cụ thể



1/ Mục tiêu thiết kế API
--


Do nhiệm vụ cơ bản của API là exposing business ra để cho client tích hợp vào, do đó API khi thiết kế ra ngoài việc đảm bảo nghiệp vụ mà nó phụ trách, còn cần đảm bảo rằng người triển khai Client dễ đọc, dễ hiểu và dễ thực hiện nhất.

Bên cạnh đó, API cần được thiết kế sao cho Client không cần chứa các business rules. Tức là việc tính toán không nằm trên client mà cần nằm trên API. Client chỉ có mục đích hiển thị dữ liệu lên.

2/ Sử dụng HTTP Method
--

- GET (SELECT): Trả về một Resource hoặc một danh sách Resource.
- POST (CREATE): Tạo mới một Resource, thường trong API POST không đi kèm Identifier của resource.
- PUT (UPDATE): Cập nhật thông tin cho Resource, toàn bộ hoặc 1 thông tin.
- DELETE (DELETE): Xoá một Resource.

Ví dụ
```
Tạo mới sản phẩm: POST api/v1/products
Cập nhật sản phẩm: PUT api/v1/products/{id}
```


3/ Cách đặt tên URL
--

Nguyên tắc
- URL cần được với version ở trước. Ví dụ ```api/v1/...```
- Sử dụng Danh từ Tiếng Anh, số nhiều cho truy vấn nhiều resources. Sử dụng lowercase và dash (kebab-case).

Ví dụ API dùng để truy vấn danh sách ví điện tử, danh sách nạp tiền và rút tiền của ví
```
api/v1/wallets
api/v1/wallet-deposits
api/v1/wallet-withdrawals
```

- Các API lấy danh sách resource như ở trên luôn phải được phân trang & hỗ trợ tìm kiếm
```
api/v1/wallets?page=1&size=10&filter="{}"
api/v1/wallet-deposits?page=1&size=10&filter="{}"
api/v1/wallet-withdrawals?page=1&size=10&filter="{}"
```

- Để lấy 1 resource cụ thể, parameter luôn phải là {id}. Không đặt là {walletId} hay {userId}
```
api/v1/wallets/{id}
```

- Nên hỗ trợ query sub-resources, ví dụ lấy danh sách nạp tiền của 1 ví điện tử như sau
```
api/v1/wallets/{id}/wallet-deposits?size=1&page=10
```

- Trong trường hợp có nhiều KEY để query 1 resource ngoài id, ví dụ orderNo thì sử dụng by-key-name để đặt URL. Chú ý các query parameter đặt dạng camelCase. 
Ví dụ dưới đây truy vấn 1 order dựa theo 3 key: orderNo, orderCode, shippingCode

```
api/v1/orders/by-order-no/{orderNo}
api/v1/orders/by-order-code/{orderCode}
api/v1/orders/by-shipping-code/{shippingCode}
```

- Sử dụng Danh từ Tiếng Anh, số ít cho Resource Type.

Ví dụ cần 1 API cấu hình cho toàn bộ hệ thống đơn hàng. Lúc này không thể dùng API URL ```api/v1/orders``` hoặc ```api/v1/orders/{id}``` được. Do API này không tác động đến toàn bộ đơn hàng hiện tại, hay đơn hàng nào cụ thể. Mà nó tác động đến loại đối tượng là Đơn hàng.

Lúc này dùng API với số ít trên URL. Nhưng lúc này cần tạo 1 controller mới là ```OrderGeneralController.cs```, với API prefix là ```api/v1/order```. Không tạo controller ```OrderTypeController``` vì dễ nhầm lẫn.

```
PUT api/v1/order/import
POST api/v1/order/types
GET api/v1/order/suggested-shipping-carriers
```

4/ HTTP Response Code 
--

Khi truy vấn API, sẽ thường có các trường hợp sau:
1. Truy vấn thành công, kết quả trả về đúng như client expected.
2. Request không hợp lệ, người dùng đang request không được xác thực hoặc dữ liệu truyền lên bị sai.

Lúc này HTTP Response Code sẽ được dùng để định nghĩa loại lỗi, giúp client xử lý và hiển thị lên

Các HTTP Response Code thường dùng như sau:

- 200 OK – Trả về khi thành công, không có lỗi nào cả.
- 401 Unauthorized – Request không được xác thực, thông thường là trường hợp người dùng chưa đăng nhập hoặc thực hiện chức năng nào đó mà không có quyền. Thường client sẽ logout ngay khi gặp code này.
- 400 Bad Request – Request không hợp lệ, field truyền lên bị sai định dạng, hoặc sai giá trị, giá trị không phù hợp.
- 403 Forbidden – Request hợp lệ, nhưng truy vấn vào resource mà user không được phép truy vấn.
- 404 Not Found – Request hợp lệ, được truy vấn resource nhưng resource không tồn tại.
- 429 Too Many Requests – Request bị từ chối do bị giới hạn số lần truy vấn trong 1 khoảng thời gian.


5/ Thiết kế Request/Response Model
--

Nguyên tắc là: 

```
Minimal Request - Maximal Response
```

Tức là:

1. Request model lên phải đơn giản nhất có thể, chỉ chứa những field cần thiết, không truyền thừa field để tránh client bị confuse.

Ví dụ: thông thường trong Token trên header đã có đủ thông tin như: userId, userName. Do đó khi tạo 1 đơn hàng ta không cần truyền các field này lên request body
```
{
    orderNo : "",
    orderItems : [],
    createdByUser: "",
    createdByUserName : ""
}
```
mà chỉ cần
```
{
    orderNo : "",
    orderItems : [],
}
```
2. Response model phải hiển thị được đầy đủ field đảm bảo client chỉ cần hiển thị là xong. Không cần thực hiện logic, cộng trừ if else mới hiển thị được.

Ví dụ, giả sử Shop đưa ra 1 chương trình là: nếu đơn hàng dưới 50 nghìn thì miễn phí ship, và thông báo lại cho khách hàng là "Miễn phí với đơn hàng dưới 50.000đ".

Lúc này API có thể trả về như dưới đây, cho đơn hàng 35k. 
```
{
    orderTotal : 35000,
    shippingCost: 5000
}
```
Sau đó dưới client phải check xem đơn có dưới 50k hay không, thì hiển thị shippingPrice = 0 thay vì = 5000. Và hiển thị message kia lên cho user.

Hoặc API có thể return về như sau và client chỉ việc hiển thị lên.
```
{
    orderTotal : 35000,
    shippingCost: 5000,
    isFreeShip : true,
    isShowMessage : true
    message : "Miễn phí với đơn hàng dưới 50.000đ"
}
```


6/ Filter Object
--
Là đối tượng dùng trong request GET để lọc dữ liệu dạng nâng cao, ở dưới dạng json stringify.

Ví dụ nếu parameter truyền lên quá nhiều key-value dẫn đến khó xử lý thì có thể truyền vào 1 string là filter với 1 object dạng json.

Thay vì 
```
api/v1/orders?size=&page=&fromDate=&toDate=&fullTextSearch=...
```
Thì sử dụng
```
api/v1/orders?size=10&page=1&filter=7B%22fromDate%22%3A%22%22%2C%20%22toDate%22%3A%22%22%7D
Decode ra là
api/v1/orders?size=10&page=1&filter={"fromDate":"", "toDate":""}
```

Các field thông thường để truy vấn 1 giá trị, nhưng cũng có thể truyền 1 mảng để truy vấn from-to. Ví dụ
```
{
    fullTextSearch : "",
    fromDate : "",
    toDate : "",
    publishDate: ["20210101", "20210103]
}
```
