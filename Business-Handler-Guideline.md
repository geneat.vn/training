CONVENTION SỬ DỤNG KHI PHÁT TRIỂN CÁC HANDLER XỬ LÝ BUSINESS LOGIC
===

Mục tiêu: tài liệu mô tả các convention khi thiết kế API dùng trong các dự án của GENEAT.

Mục lục

1. Thiết kế Handler cơ bản
2. Logic cho từng Method
6. Một số trường hợp nên và không nên



1/ Thiết kế Handler cơ bản
--

Handler là đối tượng xử lý business logic của các controller tại API.

Handler bao gồm 3 thành phần:
- Model
- Interface
- Implementation

2/ Model
--

Model là các class sử dụng như các Data Transfer Objects (DTO), là các data contract để giao tiếp giữa API, Client và Business Logic.

Với một danh mục CRUD thì một model gồm có:

- CreateModel: model sử dụng để tạo mới đối tượng
- UpdateModel: model sử dụng để sửa đối tượng
- ViewModel: model sử dụng để hiển thị thông tin đối tượng
- QueryModel: model sử dụng để query dữ liệu

Một số trường hợp khác:
- Với các model đơn giản ta có thể gộp Create và UpdateModel thành CreateUpdateModel.
- Và một số trường hợp cần chia model View thành ListView và DetailView, do ListView thường có ít field hơn DetailView để tối ưu performance
- Một số trường hợp cần chia thêm model cho User và Admin; khi User thương là public user sẽ có số field ít hơn của Admin.

Ví dụ các model cho nghiệp vụ DepositRequest (Yêu cầu kỹ quỹ)
```
- DepositRequestCreateModel
- DepositRequestUpdateModel
- DepositRequestListViewModel
- DepositRequestDetailViewModel
- DepositRequestQueryModel
```

2/ Interface
--

Interface là class contract, thường có các Method

- Create 
- Update
- GetPaged
- Delete
- GetById 

Ví dụ

```
public interface IDepositRequestHandler 
{
    Task<Response<DepositRequestViewModel>> Create(CreateDepositRequestModel model);
    Task<Response<DepositRequestViewModel>> Update(UpdateDepositRequestModel model, Guid id);
    Task<Response<DepositRequestViewModel>> Approve(Guid id);
    Task<Response<DepositRequestViewModel>> Reject(Guid id, string reason);
    Task<Response<DepositRequestViewModel>> GetById(Guid id);
    Task<Response<Pagination<DepositRequestViewModel>>> GetPageAsync(DepositRequestQueryModel query);
}
```

**CHÚ Ý**
- Các response type của các method nên cụ thể, ví dụ `Task<Response<DepositRequestViewModel>>` thay vì `Task<Response>`

2/ Implementation
--

Tạo ra implementation để implement lại interface trên.
Inject các interface khác vào ví dụ `IMapper, DbContext, IHttpContextAccessor`
Ví dụ 

```
public class DepositRequestHandler : IDepositRequestHandler
{
    private readonly IMapper _mapper;
    private readonly UbDbContext _dbContext;
    private readonly IConfiguration _config;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ConfigHandler _configHandler;

    public DepositRequestHandler(IMapper mapper, 
        UbDbContext dbContext, 
        IConfiguration config,
        ConfigHandler configHandler,
        IHttpContextAccessor httpContextAccessor)
    {
        _mapper = mapper;
        _dbContext = dbContext;
        _config = config;
        _httpContextAccessor = httpContextAccessor;
        _configHandler = configHandler;
    }
``` 
**Quy luật của các method khi implement**
- Luôn luôn thực hiện qua 2 bước
- Bước 1: Validation, thực hiện validate các thông tin như model truyền vào, dữ liệu trong database và phân quyền
- Bước 2. Thực hiện logic 

A/ Validation 
---

**Validate model truyền vào**

Thực hiện đầu tiên trong logic, các công việc cần validate như: 
- Giá trị trong model truyền vào có thỏa mãn hay không
- Giá trị có đúng hay không?
- Nếu không đúng thì trả về `BadRequest`

**Kiểm tra Tồn tại**

- Đối với các API GetById thì khi get đối tượng ra cần kiểm tra tồn tại
- Nếu null thì trả về `NotFound`

**Kiểm tra Phân quyền**

- Với các API cần phân quyền như chỉ cho phép Owner hay Admin được access thì cần check phân quyền
- Nếu không thỏa mãn phân quyền thì trả về `Forbidden`

A/ Try...Catch và Exception Handling
---

Trong mỗi method đều phải có Try Catch và Log .

Ví dụ 
```
public async Task<Response<ViewModel>> Update(Guid id, UpdateModel model)
{
    try
    {
        // Validate model
        if (model == null)
            return Helper.CreateBadRequestResponse<ViewModel>("Model is invalidated");
        
        // Validate existing
        var entity = await _dbContext.ListA.Where(x => x.Id == id).FirstOrDefaultAsync();
        if (entity == null)
            return Helper.CreateNotFoundResponse<ViewModel>("Unable to find record");

        // Validate permission
        if (currentUser.Id != entity.CreatedByUserId)
            return Helper.CreateForbiddenResponse<ViewModel>("Unable to access record");

        // Main logic


        // Save changes

        // Return
        return Helper.CreateSuccessResponse<ViewModel>(_mapper.Map<ViewModel>(entity));
    }
    catch (Exception ex)
    {
        Log.Error(ex, string.Empty);
        return Helper.CreateExceptionResponse<DepositRequestViewModel>(ex);
    }
}
```






