SQL Database Convention
===

Đây là những quy tắc áp dụng trong việc thiết kế Database và Entity.

A. Các quy tắc thiết kế Entity với Code First
===

1. **Sử dụng tiền tố viết thường, dùng gạch dưới**

ví dụ im_Account

2. **Sử dụng tiếng Anh, số ít. Không dùng số nhiều cho bảng**

ví dụ: dùng im_User thay cho im_Users

3. **Các từ phân tách nhau bằng dấu gạch dưới**

Ví dụ:

- im_User_Role
- im_Package_Price


A. Các quy tắc thiết kế Attribute với Code First
==

1. **Ưu tiên sử dụng Annotation thay cho Fluent API**

Mục đích: Việc sử dung Annotation giúp dễ định hình cấu trúc dữ liệu hơn.

Ví dụ sử dụng [MaxLength(...)] thay cho Fluent API. 

2. **Luôn luôn có Table name Annotation đặt trước Entity class**

Mục đích: giúp cho việc Rename table dễ dàng hơn

```
[Table("im_Profile")]
public class im_Profile
....
```

3. **Key field luôn đặt là Id, không đặt là ID, PaymentID, UserId**

Mục đích: ngắn gọn tên field.


Good
```
public class im_Profile
{
    [Key]
    public Guid Id { get; set; }
```

Bad
```
public class im_Profile
{
    [Key]
    public Guid ProfileId { get; set; }
    ..
    public Guid ProfileID { get; set; }
```

4. **Hạn chế sử dụng tên table trong tên field**

Mục đích: ngắn gọn tên field.

Good
```
class im_Profile
Id
Name
Value
```

Bad
```
class im_Profile
Id
ProfileName
ProfileValue
```

5. **Luôn luôn sử dụng MaxLength nếu có thể để giới hạn số ký tự của field**

Good

```
[MaxLength(50)]
string PhoneNumber {get; set;}
```

B. Quy tắt đặt tên các field
===

1. **Tên field sử dụng danh từ Tiếng Anh, Pascal Case**

Good
```
TotalPageViews, IsPurchased
```

Bad
```
total_view, isAdvisor
```

2. **Các field dạng Bit, có định dạng Yes/No, đặt là bool, tên bắt đầu với Is**

Chú ý: các field dạng BIT không đặt là INT, một số trường hợp hay dùng int với 2 giá trị 1 và 0.

Good
```
bool IsApproved;
bool IsAllowEdit;
bool IsPrivate
```

Bad
```
bool Approved;
int IsAllowEdit
```


3. **Các field ngày tháng, đặt bắt buộc có ...Date ở cuối, data type: DateTime**

Ví dụ : 
```
DateTime CreatedDate
DateTime ModifiedDate
DateTime? ActiveDate
```


4. **Các field về tiền tệ, đặt bắt buộc có Amount ở cuối, data type: decimal**

Ví dụ: 
```
decimal TransferAmount
decimal PackageAmount
decimal RefundAmount
```

5. **Các field về số lượng, đếm số, sử dụng Count ở cuối, data type: int**

Ví dụ: 
```
int EmployeeCount
int ItemCount
```

6. **Các field số lượng, đếm số hoặc tiền tệ nhưng có sự tổng hợp và liên quan với các bảng con thì dùng Total ở đầu**

Ví dụ: 
```
(data type = decimal hoặc int)

- int TotalItemCount = Tổng các ItemCount ở các bảng con
- int TotalEmployeeCount = tổng các employee count ở các bảng con
- decimal TotalInvoicePrice 
```

7. **Các field về tỉ lệ % sử dụng Percentage ở cuối, data type: float**

Ví dụ:
```
- float ProfitMarginPercentage
- float ReturnPercentage
```

8. **Tất cả các đường dẫn, link như link file, link image đặt tên field với Url ở cuối, data type = string**

Ví dụ: string FileUrl, string ThumbnailUrl, string AvatarUrl


9. **Nếu 1 filed được lưu trữ nhiều giá trị, tách nhau bởi dấu phẩy thì thêm từ khoá List vào trước tên đối tượng. Không dùng số nhiều.**

Ví dụ: 

string RoleName > RoleListName nếu lưu trữ nhiều RoleName
string RoleId -> RoleListId nếu lưu trữ nhiều RoleId
InvoiceNumber -> InvoiceListNumber
PaymentRequestCode -> PaymentRequestListCode

Tuy nhiên cách làm trên nên hạn chế và thay đổi bằng cách tạo ra bảng quan hệ phụ






