# Hướng dẫn deploy API

LINK HƯỚNG DẪN CỦA MICROSOFT: https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-nginx?view=aspnetcore-3.1


Các bước thực hiện:

1. Chuẩn bị hệ thống
1. Tạo các folder cần thiết
2. Checkout code
3. Viết script systemd
4. Viết script build API
5. Cấu  hình NginX
6. Cấu hình build tự động

# Tổng quan hệ thống Backend API

Dưới đây là mô tả hình vẽ các thành phần cơ bản của một hệ thống backend API trong thực tế:

![image](/uploads/e5e70e6cd7935544ecf5d2c2be8b8052/image.png)

Giải thích Mô hình hệ thống:

Một request đi từ Internet, hay đi từ Frontend tới Database sẽ qua các bước sau:

1. Request từ Frontend lên Internet
2. Request từ Internet qua Firewall của hệ thống Cloud (Amazon Webservice, Google Cloud)
3. Request từ Firewall Cloud đi tới Firewall Máy chủ
4. Từ Firewall máy chủ tới Reverse Proxy (NginX)
5. Từ Reverse Proxy tới Web server của .NET Core (Kestrel): Kestrel chính là tiến trình được chạy khi ta chạy lệnh ```donet ...dll```
6. Từ Kestrel tới Cơ sở dữ liệu

Vai trò của các thành phần:

- **Cloud Firewall**: là firewall của các nhà cung cấp máy chủ, các cloud như AWS, GCP. Thường là thiết bị firewall chuyên dụng, có tác dụng làm firewall, cân bằng tải, chống DDoS rất tốt. Cấu hình thông qua giao diện quản lý của nhà cung cấp. Để truy cập vào 1 port trên máy chủ, ví dụ 9001 thì trên firewall phải mở port này.

- **Server Firewall**: là firewall bằng phần mềm chạy trên máy chủ, có tác dụng làm lớp bảo vệ thứ hai tới hệ thống. Thường cài đặt ```firewalld``` làm firewall máy chủ cho dễ sử dụng. Ngoài ra có các loại khác như: ufw, hay đơn giản là dùng iptables. Để truy cập vào 1 port trên máy chủ, ví dụ 9001 thì trên firewall phải mở port này.

- **Reverse Proxy**: là thành phần hỗ trợ Web server hệ thống, thực hiện các công việc như: log request, routing request, SSL offloading, Gzip content, Cache content. Nói chung là các thao tác giúp làm giảm tải Web server của máy chủ. Reverse Proxy sẽ listen trên 1 port, sau đó chuyển tiếp các request của port này tới hệ thống api. Reverse Proxy có thể gom nhiều API lại, mặc dù các API listen trên các port khác nhau, thành 1 URL duy nhất. **Chú ý: Reverse Proxy sẽ listen trên 1 port của site, và port này được Public ra Internet, ví dụ 9001**

- **Web Server**: là web server chạy ứng dụng .NET Core, chạy đoạn code .NET Core thao tác với cơ sở dữ liệu, ghi log, trả về client. Dữ liệu khi trả về (dạng JSON, Image, hoặc HTML) sẽ đi qua Reverse Proxy và được nén lại nếu cần. **Chú ý: Web server cũng listen trên 1 port, nhưng là port nội bộ, không có HTTPS và không public ra Internet, ví dụ 5001**


## Bước 1. Chuẩn bị hệ thống

### A/ Hệ điều hành 
Hệ thống deployment cho ứng dụng API là một trong các OS sau:

- Centos 7
- Ubuntu các phiên bản LTS 18.04, 20.04

Có thể thực hành cài trước bằng cách cài trên VMWamre

Hệ thống cần được cài đặt đủ **.NET Core 3.1 SDK** tương ứng với OS hiện tại.

Hướng dẫn cài đặt cho Linux sử dụng Package Manager

https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website

### B/ Domain, firewall và các port 


Như vậy trước khi cài đặt phải thiết kế các mục sau:

- Chuẩn bị Domain
- Chuẩn bị Port

Ví dụ muốn deploy vào địa chỉ: scn.geneat.vn:9301 thì ta cần chuẩn bị:

- Tạo domain scn.geneat.vn và trỏ vào địa chỉ server
- Mở port 9301 trên firewall, cả firewall cloud và firewall server

Để mở port trên firewall server thì cài đặt firewalld và dùng các lệnh sau, tùy theo muốn mở port nào:

```
sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
sudo firewall-cmd --zone=public --add-port=9301/tcp --permanent
```

Chú ý: Trong 1 hệ thống ứng dụng ta thường dùng các site có port liền nhau cho dễ quản lý.

Ví dụ hệ thống SCN gồm có 4 site thì thiết kế như sau:

- Trang Admin Web port 9000 (URL: scn.geneat.vn:9000)
- API port 9001 (URL: scn.geneat.vn:9001)
- Web frontend port 80  (URL: scn.geneat.vn)

## Bước 2. Tạo các folder cần thiết

SSH vào hệ thống và tạo các folder sau. 

### A/ Folder tên ứng dụng:

- Đặt tên ngắn gọn
- Viết liền, không có dấu cách
- Đặt trong folder /opt/
- Ví dụ: monitor, dcs, scn, gnt

```
sudo mkdir /opt/monitor
```

### B/ Folder ứng dụng bên trong

Để dễ dàng backup thì một ứng dụng thường có các folder sau:

- Folder chứa ứng dụng được deploy lên
- Folder chứa source code để build
- Folder chứa các file static
- Folder chứa log

Trong folder ứng dụng sẽ chứa các folder con sau tùy thuộc hệ thống:
- API
- Backend
- Frontend
- Web


Do đó đặt tên các folder như sau (giả sử folder ứng dụng là monitor)

```
/opt
    /monitor
        /app    (folder này chứa ứng dụng, nếu ứng dụng dev thì thêm hậu tố .dev)
            /api
            /api.dev
            /web
            /adminweb
        /code   (folder này chứa source code)
            /monitor.code
        /file  (folder này chứa file static)
        /logs    (folder này chứa log hệ thống)
```

```
mkdir /opt/monitor/app
mkdir /opt/monitor/code
mkdir /opt/monitor/log
mkdir /opt/monitor/file

mkdir /opt/monitor/app/api
mkdir /opt/monitor/app/api.dev
mkdir /opt/monitor/app/adminweb
```

## Bước 3. Checkout code

Chúng ta sẽ sử dụng checkout code bằng SSH checkout (không dùng https)

Copy file SSH key vào folder ~/.ssh/, file sẽ ở đường dẫn ví dụ sau ``` ~/.ssh/truongnd-gitlab```

Checkout code

```
cd /opt/monitor/code
eval `ssh-agent -s`
ssh-add ~/.ssh/truongnd-gitlab

(Nhập key SSH nếu có)

git clone [SSH URL] monitor-api
```

Với SSH URL lấy từ URL của GitLab.

![image](/uploads/253e64fe1ac8459ba2cf3b8373e68487/image.png)

Sau khi checkout xong thử publish hệ thống

```
cd /opt/monitor/code/monitor.api
dotnet publish -c Release -o /opt/monitor/app/api
cd /opt/monitor/app/api
ls
```

Nếu chạy không lỗi và folder app ra như này là được

![image](/uploads/37069421f5af65545e0a4baae04da0ee/image.png)

Thử vào chạy app

```
cd /opt/monitor/app/api
dotnet [DLL NAME]
```
Trong đó DLL Name là file DLL của app được sinh ra, ví dụ monitor.api.dll

## Bước 4. Cấu hình ứng dụng cho Production

LƯU Ý QUAN TRỌNG:

- Ứng dụng Production sẽ không chạy trên HTTPS mà chỉ HTTP, do đó chú ý cấu hình source code chỉ chạy HTTP

- Trên hệ thống Production ứng dụng sẽ được chạy với file ***appsettings.Production.json*** mà không dùng cấu hình trong file **appsettings.json***. Do đó ứng dụng trong code phải được support file ***appsettings.Production.json***; nếu chỉ có support file appsettings.json sẽ không chạy được.

Tạo file production như sau:

```
cd /opt/monitor/app/api
cp appsettings.json appsettings.Production.json
```

Trên MobaXterm, vào chức năng SFTP, nhập folder ```/opt/monitor/app/api``` rồi chọn file appsettings.Production.json và sửa nội dung 

![image](/uploads/bb576aba23947eb9da9f7d441a7f1452/image.png)

Các nội dung cần sửa:

- Database Connection
- Log folder
- Static folder

Sau khi sửa xong lưu lại là xong.

## Bước 5. Tạo service systemd để chạy API tự động

Bình thường API sẽ chỉ được chạy nếu ta gõ lệnh ```dotnet run```. Để khởi chạy tự động ta tạo một service cho API.

Tên service đặt với hậu tố api đằng sau. Ví dụ: ```monitor.api.service```

Tạo file như sau:

```
sudo nano /etc/systemd/system/monitor.api.service

```

Và nhập nội dung như sau

```
[Unit]
Description=Monitor API   (***CHÚ Ý SỬA NỘI DUNG NÀY THÀNH DESCRIPTION PHÙ HỢP)

[Service]
WorkingDirectory=/opt/monitor/app/api/    (***CHÚ Ý SỬA NỘI DUNG NÀY)
ExecStart=/usr/bin/dotnet /opt/monitor/app/api/monitor.api.dll  (***CHÚ Ý SỬA NỘI DUNG NÀY)
SyslogIdentifier=monitor.api (***CHÚ Ý SỬA NỘI DUNG NÀY)
User=root
Environment=ASPNETCORE_ENVIRONMENT=Production        (***CHÚ Ý SỬA NỘI DUNG NÀY) 
Environment=ASPNETCORE_URLS=http://localhost:9900    (***CHÚ Ý SỬA NỘI DUNG NÀY)
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target

```

Sau đó bấm Ctr + O -> Enter để lưu và kích hoạt service như sau:

```
sudo systemctl enable monitor.api
sudo systemctl daemon-reload
sudo systemctl start monitor.api
```

Kiểm tra trạng thái service bằng cách

```
sudo systemctl status monitor.api
hoặc
sudo journalctl -fu monitor.api
```

Nếu thấy trạng thái Active/Running thế này là được

![image](/uploads/97d1da1a3c4984c1dff85071b67298e5/image.png)

## Bước 6. Cấu hình NginX

NginX có tác dụng làm Reverse Proxy cho hệ thống API .NET Core, phục vụ các chức năng: cache request, limit request, gzip request, SSL offloading...vv

File cấu hình Nginx tại địa chỉ: */etc/nginx/conf.d/*. Ví dụ */etc/nginx/conf.d/monitor.conf*

Mở file lên bằng nano hoặc Visual Studio Code và thêm đoạn sau

```
server {
    listen       9000; # port cho site 
    server_name  monitor.geneat.vn;  # domain

    # Load configuration files for the default server block.
    include /etc/nginx/default.d/*.conf;

    location / {
        proxy_pass http://localhost:9101;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection keep-alive;
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}

```

Sau đó Ctrl + O để write file, thoát và kiểm tra lại Nginx và reload

```
sudo nginx -t
sudo nginx -s reload
```

## Bước 7. Mở firewall port tương ứng
```
sudo firewall-cmd --zone=public --add-port=9000/tcp --permanent
sudo firewall-cmd --reload
```

Kiểm tra cổng đã mở hay chưa bằng cách telnet từ máy localhost, mở MobaXterm tab mới và gõ

```
telnet monitor.geneat.vn 9000
```

## Bước 8. Khởi động API và kiểm tra trình duyệt

Khởi động service systemd

```
systemctl start monitor.api
systemctl status monitor.api

```

Mở trình duyệt để kiểm tra swagger có hoạt động hay không.

## Bước 9. Cấu hình build tự động

Để thuận tiện trong việc build và deploy, cài đặt script sau tại folder */opt/monitor*

Tạo file build-api.sh với nội dung như sau

```
echo '- - - - - - - MONITOR DEPLOYMENT - - - - - - - - - - - - - - - - - - - - '
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
    ssh-add ~/.ssh/truongnd-gitlab
fi
echo 'Going to deploy service:' $1
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo 'Pulling code...'
cd /opt/monitor/code/monitor.api/   # CHÚ Ý ĐỂ ĐÚNG PATH
git checkout master # CHÚ Ý ĐỂ ĐÚNG BRANCH
git pull
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo 'Latest commit'
git log -n 1
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo '5 latest commits'
git log --oneline -n 5
echo ''
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
echo 'Stop services...'

cd /opt/monitor/code/monitor.api/
systemctl stop monitor.api
dotnet publish -c Release -o /opt/monitor/app/monitor.api
systemctl start monitor.api
systemctl status monitor.api
echo "Finish Monitor API deployment"
echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '

```

Thử chạy file này bằng lệnh sau và xem kết quả

```
sudo bash /opt/monitor/build-api.sh
```



